import StateLessBtn from './StateLessBtn'
import React from 'react';

const clickHandler = () => {
	console.log('My Action for Button')
}

const clickHandler2 = () => {
	console.log('My Action for Button 222')
}

const style = {
	backgroundColor: '#800080',
	width: '100px'
}

const style2 = {
	backgroundColor: '#D2691E',
	width: '200px'
}

const Classwork1 = () => {
	return(
	<div>
		<StateLessBtn
			clickHandler = {clickHandler}
			style = {style}
			textValue = "Action"
		/>
		<StateLessBtn
			clickHandler = {clickHandler2}
			style = {style2}
			textValue = "Action 2222"
		/>
		<StateLessBtn/>
	</div>
)}


export default Classwork1;