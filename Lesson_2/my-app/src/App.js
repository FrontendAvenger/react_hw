import React, { Component } from 'react';
import './App.css';
import Navigation from './navigation';
import Classwork1 from './Classwork1/index'
import Classwork2 from './Classwork2/index'
import Homework1 from './Homework1/index'
import Homework2 from './Homework2/index'

class App extends Component {
  state = {
    activeTabId: 3,
    buttons: [
      {
        id: 0,
        title: 'Classwork1',
        component: Classwork1
      },
      {
        id: 1,
        title: 'Classwork2',
        component: Classwork2
      },
      {
        id: 2,
        title: 'Homework1',
        component: Homework1
      },
      {
        id: 3,
        title: 'Homework2',
        component: Homework2
      },
    ]
  }

  handleButtonChange = (e) => {
    this.setState({
      activeTabId: Number( e.target.dataset.id )
    })
  }

  render() {
    const { handleButtonChange } = this;
    let { activeTabId, buttons } = this.state;
    return (
      <div className="wrap">
        <header className="header">
          <Navigation activeTab={activeTabId} buttons={buttons} changer={handleButtonChange}/>
        </header>
        <section className="content">
        {
          buttons.map( (tab, key) => {
            if( tab.id === activeTabId){
              let Component = tab.component;
              return(
                <Component key={key}/>
              );
            } else {
              return null;
            }

          })
        }
        </section>
       

      </div>
    );
  }
}

export default App;

