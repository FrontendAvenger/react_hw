import React from 'react';
import './index.css'
const Cell = ({style, textValue, type, currency, cells}) => {
	
	let newStyle = {...style};
	if( cells !== undefined){
		newStyle.width = `${160 * cells}px`;
	}
	return(
		<div className= {
  	        (type === "text") ? "CellStyle textStyle" :
			(type === "date") ? "CellStyle dateStyle" :
			(type === "number") ? "CellStyle numberStyle" :
			(type === "money") ? "CellStyle moneyStyle" : "CellStyle"
  		} style={newStyle}>
	    
	    { 
	      (type === "money" && !!currency) ? `${textValue}${currency}` : 
		  (type === "money" && !currency) ? console.error("There is no currency") : textValue
	    }
	  </div>
	);
}
  

Cell.defaultProps = {
  style: {
    background:"#AFEEEE",
    color:"#000000",
    border: "solid 1px",
    width: "160px",
    height: "50px",
  },
  textValue: "Some data",
  type: "text",
  cells: 1
}

export default Cell;
