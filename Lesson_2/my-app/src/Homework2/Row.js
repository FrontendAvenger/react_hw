import React from 'react';

const Row = ({ children, head }) =>{
  return(
    <div className={ head === false ? "RowStyle" : "RowStyle head"}>
    {
      children
    }
    </div>
)};


Row.defaultProps = {
  head: false
}


export default Row;

