import React from 'react';
import Row from './Row'
import Cell from './Cell'

const style = {
	background:"#FFE4C4",
	color:"#7FFFD4"
}
const Table = () => {
	return(
      <div className="TableStyle">
        
        <Row head="true">
            Head
        </Row>
        <Row>
        	<Cell cells="3" textValue="Cell1" type="text"/>
        	<Cell textValue="22/11/2019" type="date"/>
		</Row>
		<Row>
        	<Cell textValue="34" type="money" currency="$" style={{ background:"#FFE4C4" }}/>
        	<Cell textValue="102984" type="number"/>
		</Row>
		<Row>
        	<Cell textValue="310" type="money" currency="$"/>
        	<Cell textValue="Some text" type="text"/>
		</Row>
       
      </div>
    )

}
export default Table;	