import React, { Component } from 'react';
import './index.css'

const UserComponent = ( {user, interviewed, key}) => {
	return(
		<div className= {interviewed ? "item-interviewed" : "item-none-inter"}>
			{user.name}
		</div>
	)
}

export default UserComponent