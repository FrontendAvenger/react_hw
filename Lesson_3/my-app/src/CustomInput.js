import React from 'react';
import PropTypes from 'prop-types';

const CustomInput = ( {name_label, name_input, type, placeholder, value, handler, typeEvent} ) => {
	let s = {
		[typeEvent] : handler
	};
	return (
		<label className="fieldStyle">
		    <div>{name_label}</div>
		    <input
		      type={type}
		      name={name_input}
		      value={value}
		      placeholder={placeholder}
		      {...s}
		    />
  		</label>
	
	)
}

CustomInput.propTypes = {
	name_label: PropTypes.string,
	placeholder: PropTypes.string,
	value: PropTypes.any,
	type: PropTypes.oneOf(['text','password','number','button', 'file']),
	name_input : PropTypes.string,
	handler : PropTypes.func.isRequired,
	typeEvent : PropTypes.oneOf(['onChange','onClick']),
}

export default CustomInput;
