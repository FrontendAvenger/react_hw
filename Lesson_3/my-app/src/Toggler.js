import React from 'react';
import PropTypes from 'prop-types';

export const Toggler = ({children, name, label_name, changeStatus, activeToggler}) => {
	return(
		<div>
			{label_name}
			<div className="TogglerContainer">
				{
					React.Children.map(children, (ChildrenItem) =>{

							return React.cloneElement(ChildrenItem, {
								name:ChildrenItem.props.name,
								active: ChildrenItem.props.name === activeToggler ? true : false,
								action: changeStatus,
								label_name:name
							})
					
					})
				}
			</div>
		</div>
	)
}


export const TogglerItem = ({name, active, action, label_name}) =>{
	return(
		<div className= {active ? "TogglerItem active" : "TogglerItem"}
			 data-value={name}
			 data-name={label_name}
			 onClick={action}
		>
		{name}
		</div>
	)
}

TogglerItem.propTypes = {
	name: PropTypes.string.isRequired,
	action: PropTypes.func,
	active: PropTypes.bool,
	children: PropTypes.element
}