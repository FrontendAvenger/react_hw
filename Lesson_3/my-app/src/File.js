import React from 'react';

const File = ({handler, image}) => {
	return (
		<div className="fileInputs">
			<label htmlFor="file"><img src={image} width="200px" heigth="200px" /></label>
			<input type="file" onChange={handler}/>
	  	</div>
	
	)
}

export default File;