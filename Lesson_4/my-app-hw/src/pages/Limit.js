import React, {Component} from 'react';

class Limit extends Component{
	state = {
		post : null,
	}
	

	componentDidMount = () => {
		const limit_id = this.props.match.params.limitid;

		fetch("https://jsonplaceholder.typicode.com/posts")
			.then( res => res.json() )
			.then( res => {
				res.length = limit_id;
				this.setState({post:res})
		})
	}


	render = () => {
		const {post} = this.state;
		const limit_id = this.props.match.params.limitid;
		return(
			<div>
				<h2>Последние новости ({limit_id}) </h2>
				<div>
					{
						(post === null) ? 
						  (<div>Page in loading...</div>) : 
						  (<div>
						  	 <ol>
						 		{post.map( item => (<li>{item.title}</li>))}
						  	 </ol>
						   </div>)
					}
					
				</div>
			</div>
		) 
	}
}

export default Limit;