import React, { Component } from 'react';
import  {Switch, Route, Link, BrowserRouter} from 'react-router-dom';
import PostItem from './PostItem'
import NotFound from "./NotFound"
import Limit from "./Limit"

class Main extends Component {
	state = {
		post : null,
		currPosts : null,
		index : 20,
	}
	
	componentDidMount = () => {
		const {index} = this.state;

		fetch("https://jsonplaceholder.typicode.com/posts")
			.then( res => res.json() )
			.then( res => {
				this.setState({post:res, currPosts: res.slice(0,index), index : index+20 })
		})
	}
	
	addPosts = () => {
		const {post,currPosts,index} = this.state;
		this.setState( {currPosts:post.slice(0,index), index:index+20} );
	}

	render = () => {
		const {currPosts} = this.state;
		const {addPosts} = this;
		return(
			<div>
				<BrowserRouter>
					<h1><Link to="/">Главная</Link></h1>
					<h1><Link to="/limit/10">Последние новости (10)</Link></h1>
					<div>
						{
							(currPosts === null) ? 
							  (<div>Page in loading...</div>) : 
							  (<div>
							  	 <ol>
							  	 	
							  	 	<Switch>
							  	 		<Route exact path="/" render={ () => (<>{currPosts.map( (item) => 
							  	 			(<li key={item.id}><Link to={{pathname:`/posts/${item.id}`,}}>{item.title}</Link></li>))}
							  	 			<div><button onClick={addPosts}>Показать еще</button></div></>)
							  	 		}/>
								  	  	
								  	  	<Route exact path="/posts/:itemid" component={PostItem}/>

								  	  	<Route exact path="/limit/:limitid" component={Limit}/>
							  	  	</Switch>
							  	  	
							  	 </ol>
							   </div>)
						}
						
					</div>
				</BrowserRouter>
			</div>
		) 
	}
}

export default Main;