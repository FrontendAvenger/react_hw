import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './Redux/store';
import SearchInputs from './Components/SearchInputs';
import WeatherRes from './Components/WeatherRes';
import History from './Components/History';

class App extends Component {
  render() {
    return (
     <Provider store={store}>
        <div className="App">
          <h1>Super Weather App</h1>
          <SearchInputs/>
          <div className="SearchRes">
            <WeatherRes/>
            <History/>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;


