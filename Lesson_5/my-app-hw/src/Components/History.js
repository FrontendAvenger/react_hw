import React from 'react';
import { connect } from 'react-redux';

const History = ( {searchHistory, clickHandler} ) => {
	return(
	   <div className="History">
	   		<h3>History</h3>
			{
				searchHistory.map( (item,index) => (<button key={index} name={item.name} onClick={clickHandler}>{item.name}</button>))
			}
	   </div>
	)
} 

const mapStateToProps = (state, ownProps) => ({
	searchHistory : state.searchHistory
})


const mapDispatchToProps = (dispatch, ownProps) => ({
	clickHandler : (e) => {
		dispatch( {type:'CLICK_HISTORY', payload: e.target.name} )
	}
});



export default connect(mapStateToProps, mapDispatchToProps)(History);