import React from 'react';
import { connect } from 'react-redux';

const SearchInputs = ( {changeHandler, clickHandler} ) => {
	return(
	   <div className="inputStyle">
		<input type="text" placeholder="Enter city" onChange={changeHandler}/>
	   </div>
	)
} 

const mapStateToProps = (state, ownProps) => ({
	
})


const mapDispatchToProps = (dispatch, ownProps) => ({
    
    changeHandler : (e) => {
        dispatch ( {type:'DATA_REQ'} );
    		fetch(`https://api.openweathermap.org/data/2.5/weather?q=${e.target.value}&APPID=aadaa8005c6901b2b3b7cb945461dc81`)
                .then( res => res.json() )
                .then( res => {
                    dispatch({
                        type: 'DATA_RES',
                        payload: res
                    })
                    
                    if (res.cod !== "404" && res.cod !== "400"){
    					dispatch({
    	                    type: 'ADD_HISTORY',
    	                    payload: res
                    	})
                    } 

                })
   	}
});



export default connect(mapStateToProps, mapDispatchToProps)(SearchInputs);