import React from 'react';
import { connect } from 'react-redux';

const WeatherRes = ( {searchResult, searchLoaded} ) => {
	return(
	   <div className="WeatherRes">
			<h3>Search Result</h3>
			{	 
				searchLoaded === false ? 
					(
						<div> Loading ...</div>
					) :
						searchResult.cod === "404" ? 
						(<div>City not found</div>) :
						searchResult.cod === "400" ? 
						(<div></div>) :
						(
							<div className="CurWeather">
								<div>
									<img src={"https://openweathermap.org/img/w/"+searchResult.weather[0].icon+".png"} alt={searchResult.name + " Weather"} width="50" height="50"/>
								</div>
								<div>
									<p>{searchResult.name}</p>
									<p>{Math.round(searchResult.main.temp - 273.15)}&deg;C</p>
								</div>
							</div>
						)
					
			}
	   </div>
	)
} 

const mapStateToProps = (state, ownProps) => ({
	searchResult : state.searchResult,
	searchLoaded: state.searchLoaded
})


const mapDispatchToProps = (dispatch, ownProps) => ({
	
});



export default connect(mapStateToProps, mapDispatchToProps)(WeatherRes);