const initialState = {
    searchLoaded: false,
    searchResult: {},
    searchHistory: [],
  };


function reducer(state = initialState, action){
     switch(action.type){

        case 'DATA_REQ':
            return{
                ...state,
                loadedStatus: false
        }
        
        case 'DATA_RES':
            return{
                ...state,
                searchLoaded : true,
                searchResult : action.payload,

        }
        
        case 'ADD_HISTORY':
            state.searchHistory.push(action.payload);
            return{
            ...state,
            searchHistory : [...state.searchHistory]
        }

        case 'CLICK_HISTORY':

            return{
            ...state,
            searchLoaded : true,
            searchResult : state.searchHistory.find( item => { return item.name === action.payload})
        }

        

        default:
            return state;
        }
  };


export default reducer;
