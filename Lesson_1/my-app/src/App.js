import React, { Component } from 'react';
import './App.css';
import data from './guests.json'
import ListItem from './ListItem.js'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      guests : data,
    }
    this.filterUsers = this.filterUsers.bind(this);
  }

  filterUsers(e){
    let query = e.target.value.toLowerCase();
    let filterArray = data.filter( (item) => {
      let guestName = item.name.toLowerCase();
      if(  guestName.indexOf(query) !== -1 ){
        return item;
      }
    })
    this.setState({
      guests : filterArray, 
    })
  }

  render() {
    const {guests} = this.state;
    const {filterUsers} = this;
    return (
      <div>
        <div>
            <input onChange={filterUsers} placeholder="Username to search"/>
        </div>
        {guests.length !== 0 ? 
          (<ol>
          {
            guests.map( item => (<ListItem key={item.index} guest={item}/>))
          }
        </ol>) : 
          (<span>Таких гостей нет!</span>)
        }
        
      </div>
    );
  }
}

export default App;
