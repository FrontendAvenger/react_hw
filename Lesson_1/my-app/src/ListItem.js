import React, { Component } from 'react';
import './App.css';

class ListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			arrived : false,
		}
		this.CheckUser = this.CheckUser.bind(this);
	}

	CheckUser(){
		const {arrived} = this.state;
		this.setState( {
			arrived: !arrived
		})
	}

	render(){
		const {guest} = this.props;
		const {arrived} = this.state;
		const {CheckUser} = this;
		return(
			<li className={ arrived ? 'user_arrived': 'user_item'}> {guest.name} <input type="checkbox" onChange={CheckUser}/></li>	
		)
	} 
	
}

export default ListItem